// This class orchestrates calculating a number's collatz path

const PROGRESS_INTERVAL = 1000n;

export class Calculator {

    constructor(private startingNumber: bigint,
        private onProgress: (progress: bigint) => any) {
    }

    /**
     * The specific law in action
     */
    private getNextStep(number: bigint): bigint {
        if (isEven(number)) {
            return number / 2n;
        } else {
            return number * 3n + 1n;
        }
    }

    /**
     * Calculates full path of the number from startingNumber all the way to 1
     */
    calculatePathToOne(): bigint[] {

        const result: bigint[] = [];

        let currentNumber = this.startingNumber;
        let steps = 0n;
        while (currentNumber !== 1n) {

            result.push(currentNumber);
            currentNumber = this.getNextStep(currentNumber);

            steps += 1n;
            this.checkForProgress(steps);

        }

        // need to push a missing 1
        result.push(1n);

        return result;

    }

    // Call the progress callback if necessary
    private checkForProgress(currentSteps: bigint) {
        if (currentSteps % PROGRESS_INTERVAL === 0n) {
            this.onProgress(currentSteps);
        }
    }

}


function isEven(n: bigint) {
    return n % 2n === 0n;
}