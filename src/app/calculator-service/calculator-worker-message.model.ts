export interface CalculatorWorkerMessage {
    type: 'PROGRESS'|'DONE'|'READY';
    data: any;
}
