import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {CalculatorWorkerMessage} from '../calculator-service/calculator-worker-message.model';
import {BIG_NUMBER_CONFIRM_THRESHOLD} from "../app-configs";

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

  inputControl = new FormControl(Date.now().toString(), [Validators.required, Validators.pattern(/^\d+$/)]);
  /* ngModel */outputValue = '';

  private worker: Worker|null = null;

  constructor() { }

  ngOnInit(): void {
  }

  evaluateInputAsExpression() {

    const input = this.inputControl.value;
    this.promptForEval(input);
    const evaled = eval(input).toString();

    this.inputControl.patchValue(evaled);
    
  }

  onConfirm() {

    this.guardInputLength(this.inputControl.value);

    // start worker
    this.worker = new Worker(new URL('../calculator.worker', import.meta.url));
    this.outputValue = 'Calculating...';

    // parse and send value
    const inputValue = BigInt(this.inputControl.value);
    this.worker.postMessage(inputValue);

    // register a callback
    this.worker.onmessage = this.handleWorkerMessage.bind(this);

  }

  /**
   * Render the length of input,
   * e.g. 'invalid', '367 digits', etc.
   */
  renderInputLength(): string {
    if (this.inputControl.valid) {
      const valueLength = this.inputControl.value.length;
      const term = valueLength >= 1 ? 'digits' : 'digit';
      return `${valueLength} ${term}`;
    } else {
      return 'Not a Number';
    }
  }

  private handleWorkerMessage(rawMsg: any) {

    const msg = rawMsg.data as CalculatorWorkerMessage;

    if (msg.type === 'PROGRESS') {
      this.handleWorkerProgress(msg.data);
    } else if (msg.type === 'READY') {
      this.handleWorkerReady();
    } else {
      this.handleWorkerDone(msg.data);
    }

  }

  private handleWorkerProgress(data: bigint) {
    this.outputValue = `Calculating...${data} steps so far`;
  }

  private handleWorkerDone(data: string) {
    this.outputValue = data;
  }

  private handleWorkerReady() {
    this.outputValue += '\nCollecting Results...';
  }

  // Ask the user if they want to continue. If not, throw an error.
  private promptForEval(expression: string) {
    const response = confirm(`Are you sure you want to evaluate "${expression}" as a JavaScript expression?`);
    if (!response) throw new Error('Cancelled by user');
  }

  // If the input is too long, ask the user to confirm. Throws error if denied.
  private guardInputLength(input: string) {
    if (input.length > BIG_NUMBER_CONFIRM_THRESHOLD) {
      const res = confirm(`Your input has ${input.length} characters. Are you sure you want to continue?`);
      if (!res) throw new Error('Cancelled by user');
    }
  }

}
