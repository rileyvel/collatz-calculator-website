/// <reference lib="webworker" />

import {Calculator} from "./calculator-service/calculator";
import {MAX_JOINABLE_ARRAY_LENGTH} from "./app-configs";

addEventListener('message', async ({ data }) => {
  doCalculate(data);
});

/**
 * Main function of the worker
 * @param data - the bigint supplied from the front end
 */
function doCalculate(data: any) {

  // assert data is a bigint
  if (typeof data !== 'bigint') {
    return;
  }

  // calculate
  const c = new Calculator(data, (progress) => {
    postMessage({
      type: 'PROGRESS',
      data: progress
    });
  });
  const result = c.calculatePathToOne();

  renderAndSendResult(result);

}

function renderAndSendResult(result: bigint[]) {

  // Say ready first, then in 10 ms, post the result

  const rendered = renderOutput(result);
  postMessage({
    type: 'READY',
    data: null
  });

  setTimeout(() => {
    postMessage({
      type: 'DONE',
      data: rendered
    });
  }, 10);
}

function renderOutput(intPath: bigint[]): string {

  let result = `This number takes ${intPath.length} steps to reach 1.\n`;

  if (intPath.length <= MAX_JOINABLE_ARRAY_LENGTH) {
    result += 'The steps are: ' + intPath.join(' -> ');
  } else {
    console.log({result: intPath});
    result += 'Open your console (right click -> inspect) to see every single step.';
  }

  return result;

}

