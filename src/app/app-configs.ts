// Any number input that are longer than this many digits will attract an additional confirmation message.
export const BIG_NUMBER_CONFIRM_THRESHOLD = 10000;

// Maximum threshold for joining bigInt array into a string and outputting in DOM.
// Longer arrays will not be displayed in DOM, and will instead be printed to the console.
export const MAX_JOINABLE_ARRAY_LENGTH = 10000;
