# CollatzCalculator

The Collatz conjecture is one of the most mesmerizing unsolved problem in mathematics. This simple web app allows you to calculate the collatz path of arbitrarily large number down to the 4-2-1 loop.

## Tooling

Tooling is provided by Angular-cli.

## Demo

https://collatz.riley.love
